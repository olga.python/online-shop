from django.urls import path

from product.views import IndexView, add_to_cart, remove_from_cart, AboutView, ContactView, \
    SingleProductView, ProductsView, ProductsWomenView, ProductsMenView

app_name = "product"

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('about/', AboutView.as_view(), name='about'),
    path('contact/', ContactView.as_view(), name='contact'),

    path('product/<uuid:uuid>', SingleProductView.as_view(), name="single-product"),
    path('products/', ProductsView.as_view(), name="all-products"),
    path('products/women/', ProductsWomenView.as_view(), name="women-products"),
    path('products/men/', ProductsMenView.as_view(), name="men-products"),

    path('add-to-cart/<uuid:uuid>', add_to_cart, name="add-to-cart"),
    path('remove-from-cart/<uuid:uuid>', remove_from_cart, name="remove-from-cart"),

]
