# Generated by Django 4.0 on 2021-12-17 17:39

from django.db import migrations, models
import product.services.utils


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_alter_product_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='uuid',
            field=models.UUIDField(db_index=True, default=product.services.utils.generate_uuid, unique=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.TextField(blank=True, default='', max_length=500),
        ),
        migrations.AlterField(
            model_name='product',
            name='id',
            field=models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
