# Generated by Django 4.0 on 2021-12-14 16:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryMen',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('shirts', models.IntegerField()),
                ('pants', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='CategoryWomen',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('skirts', models.IntegerField()),
                ('pants', models.IntegerField()),
                ('tops', models.IntegerField()),
                ('dresses', models.IntegerField()),
                ('blouses', models.IntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='subcategorymen',
            name='category',
        ),
        migrations.RemoveField(
            model_name='subcategorywomen',
            name='category',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
        migrations.DeleteModel(
            name='SubcategoryMen',
        ),
        migrations.DeleteModel(
            name='SubcategoryWomen',
        ),
    ]
