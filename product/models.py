from django.db import models
from product.services.utils import generate_uuid


class BaseModel(models.Model):
    class Meta:
        abstract = True


class Product(BaseModel):
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)
    product_name = models.CharField(null=False, max_length=15, unique=False)
    description = models.TextField(max_length=500, default='', blank=True)
    colour = models.CharField(null=False, max_length=20, unique=False)
    price = models.FloatField(null=False, max_length=6, unique=False)
    photo = models.ImageField(upload_to="product_photos/", null=False)

    SIZE_CHOICES = (
        ('S', 'Small'),
        ('M', 'Medium'),
        ('L', 'Large')
    )
    size = models.CharField(max_length=10, choices=SIZE_CHOICES, default='M')

    CATEGORY_CHOICES = (
        ('0', 'Men'),
        ('1', 'Women')
    )
    category = models.CharField(max_length=10, choices=CATEGORY_CHOICES)
    if category == '0':
        SUBCATEGORY_CHOICES = (
            ('sh', 'Shirts'),
            ('ts', "T-Shirts"),
            ('pa', 'Pants'),
            ('n', "None")
        )
    else:
        SUBCATEGORY_CHOICES = (
            ('sk', 'Skirts'),
            ('to', "Tops"),
            ('pa', 'Pants'),
            ("dr", "Dresses"),
            ('bl', "Blouses"),
            ('N', "None")
        )

    subcategory = models.CharField(max_length=10, choices=SUBCATEGORY_CHOICES)

    def __str__(self):
        return self.product_name
