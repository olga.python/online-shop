from datetime import timezone

from django.core.checks import messages
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, TemplateView
from accounts.models import OrderProduct, Order
from .models import (Product)


class IndexView(TemplateView):
    template_name = 'index.html'
    extra_context = {
            "all_products": Product.objects.all(),
            "category_w": Product.objects.filter(category="1"),
            "category_m": Product.objects.filter(category="0")
        }


class SingleProductView(DetailView):
    model = Product
    template_name = 'single-product.html'

    def get_object(self, queryset=None):
        return Product.objects.get(uuid=self.kwargs.get("uuid"))


class ProductsView(ListView):
    model = Product
    template_name = 'products.html'
    context_object_name = "all_products"


class ProductsWomenView(ProductsView):

    def get_queryset(self):
        return Product.objects.filter(category="1")


class ProductsMenView(ProductsView):

    def get_queryset(self):
        return Product.objects.filter(category="0")


class AboutView(TemplateView):
    template_name = 'about.html'


class ContactView(TemplateView):
    template_name = 'contact.html'


def add_to_cart(request, uuid):
    product = get_object_or_404(Product, uuid=uuid)
    order_product, created = OrderProduct.objects.get_or_create(
        product=product,
        user=request.user,
        ordered=False
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)

    if order_qs.exists():
        order = order_qs[0]

        if order.products.filter(product__uuid=product.uuid).exists():
            order_product.quantity += 1
            order_product.save()
            messages.Info(request, "Added quantity Product")
            return reverse_lazy("index")
        else:
            order.products.add(order_product)
            messages.Info(request, "Product added to your cart")
            return reverse_lazy("product")
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(user=request.user, ordered_date=ordered_date)
        order.products.add(order_product)
        messages.Info(request, "Product added to your cart")
        return reverse_lazy("product")


def remove_from_cart(request, uuid):
    product = get_object_or_404(Product, uuid=uuid)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        if order.products.filter(product__uuid=product.uuid).exists():
            order_product = OrderProduct.objects.filter(
                product=product,
                user=request.user,
                ordered=False
            )[0]
            order_product.delete()
            messages.Info(request, "Product \""+order_product.product.product_name+"\" remove from your cart")
            return reverse_lazy("product", uuid=uuid)
        else:
            messages.Info(request, "This Product not in your cart")
            return reverse_lazy("product", uuid=uuid)
    else:
        messages.Info(request, "You do not have an Order")
        return reverse_lazy("product", uuid=uuid)
