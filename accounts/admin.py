from django.contrib import admin
from .models import CustomUser, OrderProduct, Order

admin.site.register(CustomUser)
admin.site.register(OrderProduct)
admin.site.register(Order)
