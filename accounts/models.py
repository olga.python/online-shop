from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models

# не получается импортнуть ugettext. Может у меня что-то не установлено необходимое, но даже при переходе на
# translation из django utils, пишется только 'gettext', 'gettext_lazy', 'gettext_noop',
#     'ngettext', 'ngettext_lazy',
#     'pgettext', 'pgettext_lazy',
#     'npgettext', 'npgettext_lazy'

from django.utils.translation import gettext as _
from accounts.managers import CustomUserManager
from accounts.validators import clean_avatar
from product.models import Product


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_("email address"), unique=True)
    first_name = models.CharField(_("first name"), max_length=150, blank=True)
    last_name = models.CharField(_("last name"), max_length=150, blank=True)
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)
    birthday = models.DateTimeField(_("birthday"), null=True, blank=True)

    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    photo = models.ImageField(upload_to="user_photos/", null=True, blank=True, validators=[clean_avatar])
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )

    objects = CustomUserManager()
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = f'{self.first_name, self.last_name}'
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)


class OrderProduct(models.Model):
    user = models.ForeignKey(to=get_user_model(), related_name="order_product", on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    product = models.ForeignKey(Product, related_name="order_product", on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.quantity} of {self.product.product_name}'

    def get_total_product_price(self):
        return self.quantity * self.product.price


class Order(models.Model):
    user = models.ForeignKey(to=get_user_model(), related_name="order", on_delete=models.CASCADE)
    products = models.ManyToManyField(OrderProduct)
    ordered_date = models.DateTimeField(default=None)
    ordered = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username
