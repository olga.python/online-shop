from django.core.exceptions import ValidationError


def clean_avatar(avatar):
    try:
        main, sub = avatar.content_type.split("/")
        if not (main == "image" and sub in ["jpeg", "gif", "png"]):
            raise ValidationError("Please use a JPEG, GIF or PNG image.")
        if len(avatar) > (20 * 1024):
            raise ValidationError("Avatar file size may not exceed 20k.")
    except AttributeError:
        pass
    return avatar
